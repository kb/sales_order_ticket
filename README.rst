Sales Order Ticket
******************

Link sales orders to tickets...

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3 venv-sales-order-ticket
  # or
  python3 -m venv venv-sales-order-ticket

  source venv-sales-order-ticket/bin/activate

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  pytest -x

Usage
=====

::

  ./init_dev.sh

Release
=======

https://www.kbsoftware.co.uk/docs/
