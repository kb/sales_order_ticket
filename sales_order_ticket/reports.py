# -*- encoding: utf-8 -*-
import attr
import logging
import pathlib
import tempfile
import urllib.parse

from datetime import date
from django.conf import settings
from django.template import loader
from django.utils import timezone
from matplotlib import cm, pyplot
from reportlab import platypus
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4
from weasyprint import HTML

from crm.models import Ticket
from invoice.analysis import time_records_to_data_frame
from invoice.models import free_of_charge_ticket_pks, TimeAnalysis, TimeRecord
from report.pdf import NumberedCanvas, PDFReport
from report.forms import ReportParametersEmptyForm
from report.models import ReportError, ReportSpecification
from report.service import ReportMixin
from sales_order.models import SalesOrder
from .models import SalesOrderTicket


logger = logging.getLogger(__name__)


@attr.s(frozen=True)
class TicketUser:
    year_month = attr.ib()
    ticket_pk = attr.ib()
    user_name = attr.ib()


def _create_plot(df, temp_folder_name, image_file_name):
    cmap = cm.get_cmap("Blues")
    # cmap = cm.get_cmap('Oranges')
    fig, ax = pyplot.subplots()
    df.plot(
        ax=ax,
        kind="pie",
        y="hours",
        autopct="%1.0f%%",
        legend=False,
        colormap=cmap,
        figsize=(2, 2),
        fontsize=6,
    )
    image_folder_file = pathlib.Path(temp_folder_name, image_file_name)
    fig.savefig(image_folder_file, dpi=200, bbox_inches="tight")


def _df_by_ticket(sales_order_df):
    df = sales_order_df.groupby(["ticket-number", "ticket"]).agg(
        {"minutes": "sum"}
    )
    df["minutes"] = (df["minutes"] / 60).round(1)
    df = df.rename(columns={"minutes": "hours"})
    return df


def _df_by_user(sales_order_df):
    df = sales_order_df.groupby(["user"]).agg({"minutes": "sum"})
    df["minutes"] = (df["minutes"] / 60).round(1)
    df = df.rename(columns={"minutes": "hours"})
    return df


def _render_template(template_file_name, data):
    template = loader.get_template(template_file_name)
    return template.render(data)


def _sales_order_df(sales_order_pk):
    """Sales order summary (as a 'pandas.DataFrame').

    Code copied from::

      sales_order_ticket/management/commands/pandas-sales-order-time-user.py

    """
    time_records = _time_records_for_sales_order(sales_order_pk)
    df = time_records_to_data_frame(time_records)
    return df


def _time_records_for_sales_order(sales_order_pk):
    ticket_pks = SalesOrderTicket.objects.filter(
        sales_order__pk=sales_order_pk
    ).values_list("ticket__pk", flat=True)
    return TimeRecord.objects.filter(
        ticket__pk__in=ticket_pks,
        date_started__gte=date(2025, 1, 1),
        date_started__lte=date.today(),  # (2025, 1, 31),
    ).order_by("date_started", "start_time")


def sales_order_time_analysis():
    result = {}
    sales_order_tickets = SalesOrderTicket.objects.current().order_by(
        "ticket__contact__user__username",
        "pk",
    )
    for sales_order_ticket in sales_order_tickets:
        # time analysis
        data = {}
        for time_record in TimeRecord.objects.filter(
            ticket=sales_order_ticket.ticket
        ).filter(date_started__gte=date(2022, 2, 1)):
            if not time_record.user.username in data:
                data[time_record.user.username] = TimeAnalysis()
            data[time_record.user.username].add(time_record)
        if data:
            contact_user_name = sales_order_ticket.ticket.contact.user.username
            if contact_user_name not in result:
                result[contact_user_name] = {}
            # e.g. '000111-Hosting'
            sales_order_description = "{}-{}".format(
                sales_order_ticket.sales_order.sales_order_number,
                sales_order_ticket.sales_order.title,
            )
            if sales_order_description not in result[contact_user_name]:
                result[contact_user_name][sales_order_description] = []
            for user_name, time_analysis in data.items():
                result[contact_user_name][sales_order_description].append(
                    (
                        sales_order_ticket.ticket.number,
                        sales_order_ticket.ticket.title,
                        user_name,
                        time_analysis.non_charge_fmt,
                        time_analysis.charge_fmt,
                    )
                )
    return result


class SalesOrderTimeAnalysisPdf(PDFReport):
    def create_pdf(self, buff, title):
        doc = platypus.SimpleDocTemplate(buff, title=title, pagesize=A4)
        elements = []

        # column widths
        column_widths = [50, 220, 100, 70, 70]
        # initial styles
        style = [
            ("BOX", (0, 0), (-1, -1), self.GRID_LINE_WIDTH, colors.gray),
            # ("LINEABOVE", (0, 0), (-1, 0), self.GRID_LINE_WIDTH, colors.gray),
            ("VALIGN", (0, 0), (0, -1), "TOP"),
            # ("ALIGN", (2, 0), (-1, -1), "RIGHT"),
        ]
        # data
        data = [
            ["Ticket", "Description", "User", "Non-Charge", "Charge"],
        ]
        report = sales_order_time_analysis()
        for contact, sales_orders in report.items():
            data.append([self._bold(contact), None, None, None, None])
            for sales_order, tickets in sales_orders.items():
                data.append([self._bold(sales_order), None, None, None, None])

                style.append(
                    (
                        "LINEBELOW",
                        (0, len(data) - 1),
                        (-1, len(data) - 1),
                        self.GRID_LINE_WIDTH,
                        colors.gray,
                    ),
                )
                style.append(
                    (
                        "SPAN",
                        (0, len(data) - 1),
                        (-1, len(data) - 1),
                    ),
                )

                for ticket_data in tickets:
                    number, title, user, non_charge, charge = ticket_data
                    data.append(
                        [number, self._para(title), user, non_charge, charge]
                    )

        # draw the table
        elements.append(
            platypus.Table(
                data, colWidths=column_widths, repeatRows=1, style=style
            )
        )
        elements.append(self._para("Printed {}".format(timezone.now())))
        # write the document to disk
        doc.build(elements, canvasmaker=NumberedCanvas)


class SalesOrderTimeAnalysis(ReportMixin):
    REPORT_FORMAT = ReportSpecification.FORMAT_PDF
    REPORT_SLUG = "sales-order-ticket-time-analysis"
    REPORT_TITLE = "Sales Order - Time Analysis"
    form_class = ReportParametersEmptyForm

    def run_pdf_report(self, buff, parameters=None):
        count = 1
        report = SalesOrderTimeAnalysisPdf()
        report.create_pdf(buff, self.REPORT_TITLE)
        return count


class TicketChargeableTimeNoSalesOrder(ReportMixin):
    REPORT_SLUG = "ticket-chargeable-time-no-sales-order"
    REPORT_TITLE = "Tickets with Chargeable Time but No Sales Order"
    form_class = ReportParametersEmptyForm

    def run_csv_report(self, csv_writer, parameters=None):
        count = 0
        csv_writer.writerow(("contact", "ticket", "description", "url"))
        ticket_pks = SalesOrderTicket.objects.check_tickets(date(2022, 2, 1))
        qs = Ticket.objects.filter(pk__in=ticket_pks).order_by(
            "contact__user__username",
            "pk",
        )
        for ticket in qs:
            count = count + 1
            absolute_url = ticket.get_absolute_url()
            url = urllib.parse.urljoin(settings.HOST_NAME, absolute_url)
            csv_writer.writerow(
                [str(ticket.contact), ticket.number, ticket.title, url]
            )
        return count

    def user_passes_test(self, user):
        return user.is_staff


class TicketFundedNoSalesOrder(ReportMixin):
    REPORT_SLUG = "ticket-funded-no-sales-order"
    REPORT_TITLE = "Funded Tickets with No Sales Order"
    form_class = ReportParametersEmptyForm

    def _tickets_since_january_2024(self):
        """Copied from 'check_tickets'..."""
        time_records = (
            TimeRecord.objects.to_invoice()
            .filter(date_started__gte=date(2024, 1, 1))
            .order_by("ticket__pk")
            .distinct("ticket__pk")
        )
        return [x.ticket.pk for x in time_records]

    def run_csv_report(self, csv_writer, parameters=None):
        count = 0
        csv_writer.writerow(("contact", "ticket", "description", "url"))
        fixed_price_ticket_pks = TimeRecord.objects.fixed_price_ticket_pks()
        free_of_charge_tickets = free_of_charge_ticket_pks()
        qs = (
            SalesOrderTicket.objects.tickets_without_sales_order()
            .filter(pk__in=self._tickets_since_january_2024())
            .exclude(pk__in=fixed_price_ticket_pks)
            .exclude(pk__in=free_of_charge_tickets)
            .order_by("contact__user__username", "pk")
        )
        for ticket in qs:
            count = count + 1
            absolute_url = ticket.get_absolute_url()
            url = urllib.parse.urljoin(settings.HOST_NAME, absolute_url)
            csv_writer.writerow(
                [str(ticket.contact), ticket.number, ticket.title, url]
            )
        return count

    def user_passes_test(self, user):
        return user.is_staff


class SalesOrderSummaryReport(ReportMixin):
    REPORT_FORMAT = ReportSpecification.FORMAT_PDF
    REPORT_SLUG = "sales-order-ticket-summary"
    REPORT_TITLE = "Sales Order - Summary Report"
    form_class = ReportParametersEmptyForm

    def _check_parameters(self, parameters):
        if not parameters:
            raise ReportError(
                f"Cannot run '{self.REPORT_SLUG}' report without any parameters"
            )
        sales_order_pk = parameters.get("sales_order_pk")
        if not sales_order_pk:
            raise ReportError(
                f"The '{self.REPORT_SLUG}' report has no 'sales_order_pk' parameter"
            )
        return sales_order_pk

    def run_pdf_report(self, buff, parameters=None):
        count = 1
        sales_order_pk = self._check_parameters(parameters)
        sales_order = SalesOrder.objects.get(pk=sales_order_pk)
        df = _sales_order_df(sales_order_pk)
        df_by_ticket = _df_by_ticket(df)
        df_by_user = _df_by_user(df)
        # sales_order_df_by_ticket = _sales_order_df_by_ticket(sales_order_pk)
        with tempfile.TemporaryDirectory() as temp_folder_name:
            image_file_name = "sales_order_by_user.png"
            _create_plot(df_by_user, temp_folder_name, image_file_name)
            data = {
                "created": timezone.now(),
                "sales_order_number": sales_order.sales_order_number,
                "summary": sales_order.summary(),
                "summary_by_user": df_by_user.to_html(),
                "summary_by_ticket": df_by_ticket.to_html(),
                "summary_by_user_plot_file_name": image_file_name,
            }
            html_out = _render_template(
                "invoice/sales_order_summary.html", data
            )
            HTML(string=html_out, base_url=temp_folder_name).write_pdf(buff)
        return count

    def user_passes_test(self, user):
        return user.is_staff


class SalesOrderTicketHoursReport(ReportMixin):
    REPORT_SLUG = "sales-order-ticket-hours"
    REPORT_TITLE = "Sales Order Ticket - Hours"

    def _check_parameters(self, parameters):
        if not parameters:
            raise ReportError(
                "Cannot run '{}' report without any parameters".format(
                    self.REPORT_SLUG
                )
            )
        sales_order_pk = parameters.get("sales_order_pk")
        if not sales_order_pk:
            raise ReportError(
                "The '{}' report has no 'sales_order_pk' parameter".format(
                    self.REPORT_SLUG
                )
            )
        return sales_order_pk

    def run_csv_report(self, csv_writer, parameters=None):
        count = 0
        sales_order_pk = self._check_parameters(parameters)
        csv_writer.writerow(
            (
                "Contact",
                "Ticket",
                "Ticket",
                "Year",
                "Month",
                "User",
                "Charge",
                # "Charge (Minutes)",
                "Fixed",
                # "Fixed (Minutes)",
                "Non-Charge",
                # "Non-Charge (Minutes)",
                "Invoiced",
                # "Invoiced (Minutes)",
                "Pending",
                # "Pending (Minutes)",
            )
        )
        data = {}
        sales_order_ticket_pks = [
            x.pk
            for x in SalesOrderTicket.objects.filter(
                sales_order__pk=sales_order_pk
            ).order_by("ticket__pk")
        ]
        for sales_order_ticket_pk in sales_order_ticket_pks:
            sales_order_ticket = SalesOrderTicket.objects.get(
                pk=sales_order_ticket_pk
            )
            time_record_pks = [
                x.pk
                for x in TimeRecord.objects.filter(
                    ticket=sales_order_ticket.ticket
                )
            ]
            for time_record_pk in time_record_pks:
                time_record = TimeRecord.objects.get(pk=time_record_pk)
                if time_record.is_complete:
                    count = count + 1
                    ticket = sales_order_ticket.ticket
                    year_month = date(
                        time_record.date_started.year,
                        time_record.date_started.month,
                        1,
                    )
                    key = TicketUser(
                        year_month=year_month,
                        ticket_pk=ticket.pk,
                        user_name=time_record.user.username,
                    )
                    if not key in data:
                        data[key] = TimeAnalysis()
                    data[key].add(time_record)
        for ticket_user, time_analysis in data.items():
            ticket = Ticket.objects.get(pk=ticket_user.ticket_pk)
            csv_writer.writerow(
                [
                    ticket.contact.full_name,
                    ticket.number,
                    ticket.title,
                    ticket_user.year_month.year,
                    ticket_user.year_month.month,
                    ticket_user.user_name,
                    time_analysis.charge_fmt,
                    # time_analysis.charge,
                    time_analysis.fixed_fmt,
                    # time_analysis.fixed,
                    time_analysis.non_charge_fmt,
                    # time_analysis.non_charge,
                    time_analysis.invoiced_fmt,
                    # time_analysis.invoiced,
                    time_analysis.pending_fmt,
                    # time_analysis.pending,
                ]
            )
        return count

    def user_passes_test(self, user):
        return user.is_staff
