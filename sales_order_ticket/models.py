# -*- encoding: utf-8 -*-
import logging

from django.db import models
from django.conf import settings
from reversion import revisions as reversion

from base.model_utils import TimedCreateModifyDeleteModel
from crm.models import Ticket
from sales_order.models import SalesOrder


logger = logging.getLogger(__name__)


class SalesOrderTicketError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))


class SalesOrderTicketManager(models.Manager):
    def _create_sales_order_ticket(self, ticket, sales_order, user):
        x = self.model(ticket=ticket, sales_order=sales_order, user=user)
        x.save()
        return x

    def check_tickets(self, from_date=None):
        """Check tickets are set-up correctly with sales orders."""
        result = []
        from invoice.models import TimeRecord

        time_records = (
            TimeRecord.objects.to_invoice()
            .order_by("ticket__pk")
            .distinct("ticket__pk")
        )
        if from_date:
            time_records = time_records.filter(date_started__gte=from_date)
        for x in time_records:
            try:
                SalesOrderTicket.objects.get(ticket=x.ticket)
            except SalesOrderTicket.DoesNotExist:
                result.append(x.ticket.pk)
        return result

    def current(self, contact=None, sales_order=None):
        qs = self.model.objects.exclude(deleted=True)
        if sales_order:
            qs = qs.filter(sales_order=sales_order)
        elif contact:
            qs = qs.filter(sales_order__contact=contact)
        return qs

    def for_ticket(self, ticket):
        result = None
        try:
            result = self.get(ticket=ticket)
        except self.model.DoesNotExist:
            pass
        return result

    def init_sales_order_ticket(self, ticket, sales_order, user):
        try:
            x = self.model.objects.get(ticket=ticket)
            if x.sales_order == sales_order:
                pass
            else:
                x.sales_order = sales_order
                x.user = user
                x.save()
        except self.model.DoesNotExist:
            x = self._create_sales_order_ticket(ticket, sales_order, user)
        return x

    def tickets_without_sales_order(self):
        """Tickets without a sales order, but with chargeable time.

        .. tip:: This method is in the ``SalesOrderTicketManager`` class.

        """
        from invoice.models import TimeRecord

        time_records = (
            TimeRecord.objects.to_invoice()
            .order_by("ticket__pk")
            .distinct("ticket__pk")
        )
        ticket_pks = [x.ticket.pk for x in time_records]
        ticket_pks_with_sales_order = set(
            self.current().values_list("ticket__pk", flat=True)
        )
        return Ticket.objects.filter(pk__in=ticket_pks).exclude(
            pk__in=ticket_pks_with_sales_order
        )


@reversion.register()
class SalesOrderTicket(TimedCreateModifyDeleteModel):
    """Sales order for a ticket."""

    sales_order = models.ForeignKey(SalesOrder, on_delete=models.CASCADE)
    ticket = models.OneToOneField(Ticket, on_delete=models.CASCADE)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="+", on_delete=models.CASCADE
    )
    objects = SalesOrderTicketManager()

    class Meta:
        ordering = ("-sales_order__pk", "-ticket__pk")
        verbose_name = "Sales Order Ticket"
        verbose_name_plural = "Sales Order Tickets"

    def __str__(self):
        return "{} {} for {} {}".format(
            self.sales_order.number,
            self.sales_order.title,
            self.ticket.number,
            self.ticket.title,
        )
