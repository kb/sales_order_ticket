from datetime import time
from report.pdf import PDFReport, NumberedCanvas
from reportlab import platypus
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4

from invoice.models import TimeRecord, TimeAnalysis
from sales_order.models import SalesOrder

from .models import SalesOrderTicket


class SalesOrderTimeReport(PDFReport):
    def _get_column_styles(self, column_widths):
        # style - add vertical grid lines
        style = []
        for idx in range(len(column_widths) - 1):
            style.append(
                (
                    "LINEAFTER",
                    (idx, 0),
                    (idx, -1),
                    self.GRID_LINE_WIDTH,
                    colors.gray,
                )
            )
        return style

    def _table(self, data, column_widths):
        style = [
            ("ALIGN", (1, 0), (-1, -1), "RIGHT"),
            ("VALIGN", (0, 0), (0, -1), "TOP"),
            ("VALIGN", (1, 0), (-1, -1), "TOP"),
            ("LINEABOVE", (0, 0), (-1, 0), self.GRID_LINE_WIDTH, colors.black),
            ("LINEBELOW", (0, 0), (-1, -1), self.GRID_LINE_WIDTH, colors.black),
            (
                "LINEBEFORE",
                (0, 0),
                (-1, -1),
                self.GRID_LINE_WIDTH,
                colors.black,
            ),
            ("LINEAFTER", (0, 0), (-1, -1), self.GRID_LINE_WIDTH, colors.black),
        ]
        style = style + self._get_column_styles(column_widths)
        return platypus.Table(
            data,
            colWidths=column_widths,
            repeatRows=1,
            style=style,
        )

    def create_pdf(self, sales_order_pk):
        doc = platypus.SimpleDocTemplate(
            "sales-order-time-report.pdf",
            title="Sales Order {}".format(sales_order_pk),
            pagesize=A4,
        )
        elements = []
        elements.append(
            self._table(
                self.sales_order_table_data(sales_order_pk),
                column_widths=[75, 100, 300, 75],
            )
        )
        elements.append(self.sep())
        elements.append(self.sep())
        elements.append(
            self._table(
                self.ticket_table_data(sales_order_pk),
                column_widths=[40, 300, 70, 70, 70],
            )
        )
        elements.append(self.sep())
        elements.append(self.sep())
        elements.append(
            self._table(
                self.user_table_data(sales_order_pk),
                column_widths=[40, 300, 70, 70, 70],
            )
        )
        doc.build(elements, canvasmaker=NumberedCanvas)

    def ticket_table_data(self, sales_order_pk):
        result = self.data(sales_order_pk)
        data = [["No", self._para("Description"), "Non", "Fix", "Chg"]]
        lines = []
        ticket_info = result["data"][sales_order_pk]["tickets"]
        sales_order_total = result["data"][sales_order_pk]["total"]
        for ticketpk, ticket_data in ticket_info.items():
            time_analysis = ticket_data["total"]
            if time_analysis == TimeAnalysis():
                pass
            else:
                lines.append(
                    [
                        ticketpk,
                        self._para(ticket_data["ticket_title"]),
                        time_analysis.non_charge_fmt,
                        time_analysis.fixed_fmt,
                        time_analysis.charge_fmt,
                    ]
                )
        total_time_analysis = sales_order_total
        lines.append(
            [
                "",
                self._bold("Total Time"),
                self._bold(
                    total_time_analysis.non_charge_fmt, align=PDFReport.RIGHT
                ),
                self._bold(
                    total_time_analysis.fixed_fmt, align=PDFReport.RIGHT
                ),
                self._bold(
                    total_time_analysis.charge_fmt, align=PDFReport.RIGHT
                ),
            ]
        )
        return data + lines

    def user_table_data(self, sales_order_pk):
        result = self.data_user(sales_order_pk)
        data = [["", self._para("User"), "Non", "Fix", "Chg"]]
        lines = []
        user_info = result["data"][sales_order_pk]["users"]
        sales_order_total = result["data"][sales_order_pk]["total"]
        for username, user_data in user_info.items():
            time_analysis = user_data["total"]
            lines.append(
                [
                    "",
                    self._para(username),
                    time_analysis.non_charge_fmt,
                    time_analysis.fixed_fmt,
                    time_analysis.charge_fmt,
                ]
            )
        total_time_analysis = sales_order_total
        lines.append(
            [
                "",
                self._bold("Total Time"),
                self._bold(
                    total_time_analysis.non_charge_fmt, align=PDFReport.RIGHT
                ),
                self._bold(
                    total_time_analysis.fixed_fmt, align=PDFReport.RIGHT
                ),
                self._bold(
                    total_time_analysis.charge_fmt, align=PDFReport.RIGHT
                ),
            ]
        )
        return data + lines

    def data_user(self, sales_order_pk):
        sales_order = SalesOrder.objects.get(pk=sales_order_pk)
        sales_order_tickets = SalesOrderTicket.objects.filter(
            sales_order=sales_order
        )
        data = {
            "data": {
                sales_order.pk: {
                    "users": {},
                    "total": TimeAnalysis(),
                }
            }
        }

        for x in sales_order_tickets:
            time_records = TimeRecord.objects.filter(ticket=x.ticket)
            for time_record in time_records:
                user = time_record.user.username
                if user in data["data"][sales_order_pk]["users"]:
                    pass
                else:
                    data["data"][sales_order_pk]["users"][user] = {
                        "total": TimeAnalysis(),
                    }
                total = data["data"][sales_order_pk]["users"][user]["total"]
                totaltime = data["data"][sales_order_pk]["total"]
                # for time in user_tickets:
                total.add(time_record)
                totaltime.add(time_record)
        return data

    def data(self, sales_order_pk):
        sales_order = SalesOrder.objects.get(pk=sales_order_pk)
        sales_order_tickets = SalesOrderTicket.objects.filter(
            sales_order=sales_order
        )
        data = {
            "data": {sales_order.pk: {"tickets": {}, "total": TimeAnalysis()}}
        }
        for sales_order_ticket in sales_order_tickets:
            time_records = TimeRecord.objects.filter(
                ticket_id=sales_order_ticket.ticket.pk
            )
            data["data"][sales_order.pk]["tickets"][
                sales_order_ticket.ticket.pk
            ] = {
                "ticket_title": sales_order_ticket.ticket.title,
                "ticket_description": sales_order_ticket.ticket.description,
                "total": TimeAnalysis(),
            }
            ticket_total = data["data"][sales_order.pk]["tickets"][
                sales_order_ticket.ticket.pk
            ]["total"]
            total = data["data"][sales_order.pk]["total"]
            for x in time_records:
                ticket_total.add(x)
                total.add(x)
        return data

    def sales_order_table_data(self, sales_order_pk):
        sales_order = SalesOrder.objects.get(pk=sales_order_pk)
        return [
            [
                "Sales Order",
                "Created",
                self._para("Title"),
                self._para("Created By"),
            ],
            [
                sales_order.sales_order_number,
                sales_order.created.strftime("%d/%m/%Y"),
                self._para(sales_order.title),
                self._para(sales_order.user.username),
            ],
        ]
