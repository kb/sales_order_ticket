# -*- encoding: utf-8 -*-
from django import forms

from crm.models import Ticket
from sales_order.models import SalesOrder


class SalesOrderTicketForm(forms.ModelForm):
    sales_order = forms.ModelChoiceField(
        label="Sales Order", queryset=SalesOrder.objects.none(), required=True
    )

    def __init__(self, *args, **kwargs):
        ticket = kwargs.pop("ticket")
        sales_order_pk = kwargs.pop("sales_order_pk")
        super().__init__(*args, **kwargs)
        f = self.fields["sales_order"]
        f.queryset = SalesOrder.objects.current(ticket.contact)
        f.initial = sales_order_pk
        f.widget.attrs.update({"class": "chosen-select pure-input-1-2"})

    class Meta:
        model = Ticket
        fields = ("sales_order",)
