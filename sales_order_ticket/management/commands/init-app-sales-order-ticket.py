# -*- encoding: utf-8 -*-
from django_rich.management import RichCommand

from sales_order_ticket.reports import (
    SalesOrderSummaryReport,
    SalesOrderTicketHoursReport,
    TicketChargeableTimeNoSalesOrder,
    TicketFundedNoSalesOrder,
)


class Command(RichCommand):
    help = "Initialist sales order ticket app"

    def handle(self, *args, **options):
        self.console.print(f"[bold blue]{self.help}")
        self.console.print(SalesOrderSummaryReport().init_report())
        self.console.print(SalesOrderTicketHoursReport().init_report())
        self.console.print(TicketChargeableTimeNoSalesOrder().init_report())
        self.console.print(TicketFundedNoSalesOrder().init_report())
        self.console.print(f"[bold green]{self.help} - Complete")
