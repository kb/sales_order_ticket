# -*- encoding: utf-8 -*-
import pandas

from datetime import date
from dateutil.relativedelta import relativedelta, MO, SU
from django.core.management.base import BaseCommand
from django.utils import timezone
from rich.pretty import pprint

from invoice.analysis import time_records_to_data_frame
from invoice.models import TimeRecord
from sales_order_ticket.models import SalesOrderTicket


def display_data_frame(caption, df):
    print()
    print(caption)
    pprint(df, expand_all=True)


def get_last_week():
    last_monday = date.today() + relativedelta(weeks=-1, weekday=MO)
    last_sunday = last_monday + relativedelta(weekday=SU)
    print("last_monday", last_monday, "last_sunday", last_sunday)
    return last_monday, last_sunday


def get_time_records_from_to(from_date, to_date):
    # last_monday = date.today() + relativedelta(weeks=-1, weekday=MO)
    # last_sunday = last_monday + relativedelta(weekday=SU)
    # print("last_monday", last_monday, "last_sunday", last_sunday)
    return TimeRecord.objects.filter(
        date_started__gte=from_date, date_started__lte=to_date
    ).order_by("date_started", "start_time")


def get_time_records_for_sales_order(sales_order_pk):
    ticket_pks = SalesOrderTicket.objects.filter(
        sales_order__pk=sales_order_pk
    ).values_list("ticket__pk", flat=True)
    return TimeRecord.objects.filter(
        ticket__pk__in=ticket_pks,
        date_started__gte=date(2025, 1, 1),
        date_started__lte=date.today(),  # (2025, 1, 31),
    ).order_by("date_started", "start_time")


class Command(BaseCommand):
    """Sales order time summary by user.

    Code based on::

      invoice/management/commands/pandas-invoice-summary.py

    """

    help = "Sales order time summary by user (with Pandas) #1017"

    def add_arguments(self, parser):
        parser.add_argument("sales_order_id", type=int, nargs="?")

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}")
        sales_order_pk = options.get("sales_order_id")
        file_name_time = timezone.now().strftime("%Y-%m-%d")
        file_name_caption = ""
        if sales_order_pk:
            file_name_caption = f"sales-order-{sales_order_pk}"
            time_records = get_time_records_for_sales_order(sales_order_pk)
        else:
            from_date, to_date = get_last_week()
            time_records = get_time_records_from_to(from_date, to_date)
            from_str = from_date.strftime("%a-%Y-%m-%d")
            to_str = to_date.strftime("%a-%Y-%m-%d")
            file_name_caption = f"{from_str}-{to_str}"
        df = time_records_to_data_frame(time_records)
        # add a column which combines the sales order and ticket information.
        # We will use this column to analyse the work done this week.
        df["sales-order-or-ticket"] = (
            "S" + df["sales-order-number"] + ". " + df["sales-order"]
        )
        df.loc[df["sales-order-number"] == "", "sales-order-or-ticket"] = (
            "T" + df["ticket-number"] + ". " + df["ticket"]
        )
        # display_data_frame("Time records since October 2024", df)
        # file_name = f"{file_name_time}-df.xlsx"
        # with pandas.ExcelWriter(file_name) as writer:
        #    df.to_excel(writer, sheet_name="DF")

        # Work done for sales orders (or ticket if no sales order)
        sales_order_df = df.groupby(
            ["contact", "sales-order-or-ticket", "user"]
        ).agg({"minutes": "sum"})
        display_data_frame("Sales order analysis", sales_order_df)
        file_name = (
            f"{file_name_time}-{file_name_caption}-"
            "contact-sales-order-user.xlsx"
        )
        with pandas.ExcelWriter(file_name) as writer:
            sales_order_df.to_excel(
                writer,
                sheet_name=(
                    f"Sales order time summary by user {file_name_caption}"
                ),
            )
        # Work done for sales orders by ticket and user
        sales_order_ticket_df = df.groupby(
            ["contact", "ticket-number", "ticket", "user"], as_index=False
        ).agg({"minutes": "sum"})
        display_data_frame("Sales order analysis", sales_order_ticket_df)
        file_name = (
            f"{file_name_time}-{file_name_caption}-contact-ticket-user.xlsx"
        )

        with pandas.ExcelWriter(file_name) as writer:
            sales_order_ticket_df.to_excel(
                writer,
                sheet_name=(
                    "Sales order time summary by "
                    f"contact, ticket and user {file_name_caption}"
                ),
            )

        # Work done for clients
        contact_df = df.groupby(["contact", "user"]).agg({"minutes": "sum"})
        display_data_frame("Contact analysis", contact_df)
        file_name = f"{file_name_time}-{file_name_caption}-contact-user.xlsx"
        with pandas.ExcelWriter(file_name) as writer:
            contact_df.to_excel(
                writer,
                sheet_name=f"Contact time summary by user {file_name_caption}",
            )
        # Work done by user
        user_df = df.groupby(["user"]).agg({"minutes": "sum"})
        display_data_frame("User analysis", user_df)
        file_name = f"{file_name_time}-{file_name_caption}-user.xlsx"
        with pandas.ExcelWriter(file_name) as writer:
            user_df.to_excel(
                writer, sheet_name=f"User time summary {file_name_caption}"
            )

        file_name = f"{file_name_time}-{file_name_caption}-summary.html"
        # sales_order_ticket_df.style.format(precision=1, thousands=".", decimal=",")
        sales_order_ticket_df.style.highlight_max()
        user_df.style.highlight_max()
        with open(file_name, "w") as f:
            sales_order_ticket_df.style.to_html(f)
            user_df.style.set_properties(
                **{"border": "1.3px solid green", "color": "magenta"}
            ).to_html(f)
            # user_df.style.to_html(f)

        self.stdout.write(f"{self.help} - Complete - see\n{file_name}")
