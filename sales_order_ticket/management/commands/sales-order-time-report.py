# -*- encoding: utf-8 -*-
import collections
import csv
import tempfile

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.utils import timezone

from contact.models import Contact
from invoice.models import TimeRecord
from sales_order_ticket.pdf import SalesOrderTimeReport


class Command(BaseCommand):
    def handle(self, *args, **options):
        table_data = []

        pdf = SalesOrderTimeReport()
        sales_order_pk = 34
        # pdf.data(name)
        pdf.create_pdf(sales_order_pk)
        return
