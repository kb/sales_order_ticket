# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.db import transaction
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import ListView, UpdateView

from base.url_utils import url_with_querystring
from base.view_utils import BaseMixin
from chat.tasks import send_chat_messages
from crm.models import Ticket
from report.models import ReportError, ReportSpecification
from sales_order.forms import SalesOrderEmptyForm
from sales_order.models import SalesOrder
from .forms import SalesOrderTicketForm
from .models import SalesOrderTicket
from .reports import SalesOrderSummaryReport, SalesOrderTicketHoursReport


class SalesOrderReportScheduleView(
    LoginRequiredMixin,
    BaseMixin,
    UpdateView,
):
    """Create a new schedule for this report specification.

    Adapted from the ``ReportSpecificationScheduleMixin``

    """

    form_class = SalesOrderEmptyForm
    model = SalesOrder
    template_name = "sales_order/sales_order_report_form.html"

    def _report_specification(self):
        try:
            return ReportSpecification.objects.get(
                slug=SalesOrderTicketHoursReport.REPORT_SLUG
            )
        except ReportSpecification.DoesNotExist:
            raise ReportError(
                f"Report '{SalesOrderTicketHoursReport.REPORT_SLUG}' does not exist"
            )

    def form_valid(self, form):
        report_specification = self._report_specification()
        with transaction.atomic():
            self.schedule = report_specification.schedule(
                self.request.user, parameters={"sales_order_pk": self.object.pk}
            )
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return url_with_querystring(
            reverse("report.schedule.csv.view", args=[self.schedule.pk]),
            next=reverse("sales.order.detail", args=[self.object.pk]),
        )


class SalesOrderReportSummaryView(LoginRequiredMixin, BaseMixin, UpdateView):
    """Create a new schedule for this report specification.

    Adapted from the ``SalesOrderReportScheduleView``

    """

    form_class = SalesOrderEmptyForm
    model = SalesOrder
    template_name = "sales_order/sales_order_report_form.html"

    def _report_specification(self):
        try:
            return ReportSpecification.objects.get(
                slug=SalesOrderSummaryReport.REPORT_SLUG
            )
        except ReportSpecification.DoesNotExist:
            raise ReportError(
                f"Report '{SalesOrderSummaryReport.REPORT_SLUG}' does not exist"
            )

    def form_valid(self, form):
        report_specification = self._report_specification()
        with transaction.atomic():
            self.schedule = report_specification.schedule(
                self.request.user, parameters={"sales_order_pk": self.object.pk}
            )
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return url_with_querystring(
            reverse("report.schedule.pdf.view", args=[self.schedule.pk]),
            next=reverse("sales.order.detail", args=[self.object.pk]),
        )


class SalesOrderTicketListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    model = SalesOrderTicket
    paginate_by = 10
    template_name = "sales_order_ticket/sales_order_detail.html"

    def _sales_order(self):
        sales_order_pk = self.kwargs["sales_order_pk"]
        return SalesOrder.objects.get(pk=sales_order_pk)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(sales_order=self._sales_order()))
        return context

    def get_queryset(self):
        sales_order = self._sales_order()
        return SalesOrderTicket.objects.current(sales_order=sales_order)


class SalesOrderTicketUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    model = Ticket
    form_class = SalesOrderTicketForm
    template_name = "sales_order_ticket/sales_order_ticket_update_form.html"

    def form_valid(self, form):
        sales_order = form.cleaned_data.get("sales_order")
        with transaction.atomic():
            self.object = form.save(commit=False)
            SalesOrderTicket.objects.init_sales_order_ticket(
                self.object, sales_order, self.request.user
            )
            self.object.chat(
                ":link: Linked sales order, {}".format(sales_order.title),
                self.request.user,
            )
            transaction.on_commit(lambda: send_chat_messages.send())
        return HttpResponseRedirect(self.get_success_url())

    def get_form_kwargs(self):
        result = super().get_form_kwargs()
        sales_order_pk = None
        sales_order_ticket = SalesOrderTicket.objects.for_ticket(self.object)
        if sales_order_ticket:
            sales_order_pk = sales_order_ticket.sales_order.pk
        result.update(dict(ticket=self.object, sales_order_pk=sales_order_pk))
        return result
