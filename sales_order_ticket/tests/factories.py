# -*- encoding: utf-8 -*-
import factory

from crm.tests.factories import TicketFactory
from login.tests.factories import UserFactory
from sales_order.tests.factories import SalesOrderFactory
from sales_order_ticket.models import SalesOrderTicket


class SalesOrderTicketFactory(factory.django.DjangoModelFactory):
    sales_order = factory.SubFactory(SalesOrderFactory)
    ticket = factory.SubFactory(TicketFactory)
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = SalesOrderTicket
