# -*- encoding: utf-8 -*-
from django.urls import re_path

from .views import (
    SalesOrderReportScheduleView,
    SalesOrderReportSummaryView,
    SalesOrderTicketListView,
    SalesOrderTicketUpdateView,
)


urlpatterns = [
    re_path(
        r"^sales/order/(?P<sales_order_pk>\d+)/$",
        view=SalesOrderTicketListView.as_view(),
        name="sales.order.detail",
    ),
    re_path(
        r"^sales/order/(?P<pk>\d+)/report/$",
        view=SalesOrderReportScheduleView.as_view(),
        name="sales.order.report",
    ),
    re_path(
        r"^sales/order/(?P<pk>\d+)/report/summary/$",
        view=SalesOrderReportSummaryView.as_view(),
        name="sales.order.report.summary",
    ),
    re_path(
        r"^sales/order/ticket/(?P<pk>\d+)/update/$",
        view=SalesOrderTicketUpdateView.as_view(),
        name="sales.order.ticket.update",
    ),
]
