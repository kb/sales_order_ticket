# -*- encoding: utf-8 -*-
import pytest
from datetime import date, datetime, timedelta, time

from django.core.management import call_command
from django.db import IntegrityError

from contact.tests.factories import ContactFactory
from crm.models import Funded, KanbanCard, KanbanColumn
from crm.tests.factories import FundedFactory, KanbanCardFactory, TicketFactory
from login.tests.factories import UserFactory
from invoice.tests.factories import TimeRecordFactory
from invoice.models import TimeRecord, TimeAnalysis
from sales_order.tests.factories import SalesOrderFactory
from sales_order_ticket.pdf import SalesOrderTimeReport
from sales_order_ticket.models import SalesOrderTicket
from sales_order_ticket.tests.factories import SalesOrderTicketFactory


@pytest.mark.django_db
def test_check_no_sales_order_chargeable_time():
    """A ticket with chargeable time, must have a sales order."""
    user_1 = UserFactory(username="user_1")
    user_2 = UserFactory(username="user_2")
    user_3 = UserFactory(username="user_3")
    user_4 = UserFactory(username="user_4")
    contact = ContactFactory(user=user_1)
    sales_order = SalesOrderFactory(contact=contact, title="Ryan", user=user_3)
    ticket_1 = TicketFactory(
        contact=contact,
        title="Orange",
        description="Orange 1",
        user=user_4,
    )
    ticket_2 = TicketFactory(
        contact=contact,
        title="Apple",
        description="Apple 1",
        user=user_4,
    )
    TicketFactory(contact=contact, title="Banana", user=user_4)
    SalesOrderTicketFactory(
        ticket=ticket_1, sales_order=sales_order, user=user_4
    )
    SalesOrderTicketFactory(
        ticket=ticket_2, sales_order=sales_order, user=user_3
    )
    TimeRecordFactory(
        ticket=ticket_1,
        date_started=date(2017, 1, 14),
        start_time=time(11, 0),
        end_time=time(11, 10),
        user=user_1,
    )
    TimeRecordFactory(
        ticket=ticket_2,
        date_started=date(2017, 1, 14),
        start_time=time(11, 15),
        end_time=time(11, 30),
        user=user_2,
    )
    TimeRecordFactory(
        ticket=ticket_1,
        date_started=date(2017, 1, 14),
        start_time=time(11, 30),
        end_time=time(11, 40),
        user=user_3,
    )
    TimeRecordFactory(
        ticket=ticket_1,
        date_started=date(2017, 1, 14),
        start_time=time(11, 30),
        end_time=time(11, 45),
        user=user_4,
    )
    TimeRecordFactory(
        ticket=ticket_1,
        date_started=date(2017, 1, 14),
        start_time=time(11, 30),
        end_time=time(11, 45),
        user=user_4,
    )
    sales_order_time_report = SalesOrderTimeReport()
    data = sales_order_time_report.data(sales_order.pk)
    # import prettyprinter

    # prettyprinter.prettyprinter.install_extras(["attrs"])
    # prettyprinter.cpprint(data)
    assert {
        "data": {
            sales_order.pk: {
                "tickets": {
                    ticket_2.pk: {
                        "ticket_title": "Apple",
                        "ticket_description": "Apple 1",
                        "total": TimeAnalysis(charge=15.0, pending=15.0),
                    },
                    ticket_1.pk: {
                        "ticket_title": "Orange",
                        "ticket_description": "Orange 1",
                        "total": TimeAnalysis(charge=50.0, pending=50.0),
                    },
                },
                "total": TimeAnalysis(charge=65.0, pending=65.0),
            },
        }
    } == data
    user_data = sales_order_time_report.data_user(sales_order.pk)
    # import prettyprinter
    # prettyprinter.prettyprinter.install_extras(["attrs"])
    # prettyprinter.cpprint(user_data)
    assert {
        "data": {
            sales_order.pk: {
                "users": {
                    "user_2": {
                        "total": TimeAnalysis(charge=15.0, pending=15.0),
                    },
                    "user_3": {
                        "total": TimeAnalysis(charge=10.0, pending=10.0),
                    },
                    "user_4": {
                        "total": TimeAnalysis(charge=30.0, pending=30.0),
                    },
                    "user_1": {
                        "total": TimeAnalysis(charge=10.0, pending=10.0),
                    },
                },
                "total": TimeAnalysis(charge=65.0, pending=65.0),
            },
        }
    } == user_data
