# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from chat.models import Channel, Message
from chat.tests.factories import ChannelFactory
from contact.tests.factories import ContactFactory
from crm.tests.factories import TicketFactory
from login.tests.factories import TEST_PASSWORD, UserFactory
from sales_order.tests.factories import SalesOrderFactory
from sales_order_ticket.models import SalesOrderTicket


@pytest.mark.django_db
def test_sales_order_ticket_update(client):
    ChannelFactory(slug=Channel.ACTIVITY)
    contact = ContactFactory(company_name="KB")
    ticket = TicketFactory(contact=contact, title="Apple")
    sales_order = SalesOrderFactory(number=1, title="Orange", contact=contact)
    user = UserFactory(is_staff=True, username="patrick")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    assert 0 == SalesOrderTicket.objects.count()
    response = client.post(
        reverse("sales.order.ticket.update", args=[ticket.pk]),
        data={"sales_order": sales_order.pk},
    )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("ticket.detail", args=[ticket.pk]) == response.url
    assert 1 == SalesOrderTicket.objects.count()
    sales_order_ticket = SalesOrderTicket.objects.first()
    assert ticket == sales_order_ticket.ticket
    assert sales_order == sales_order_ticket.sales_order
    assert user == sales_order_ticket.user
    assert 1 == Message.objects.count()
    message = Message.objects.first()
    assert (
        "[Ticket #{}](http://localhost:8000/ticket/{}/), "
        "KB, Apple, :link: Linked sales order, Orange "
        "(by *patrick*)".format(ticket.pk, ticket.pk)
    ) == message.title
