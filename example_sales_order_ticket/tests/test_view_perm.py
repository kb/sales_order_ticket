# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from contact.tests.factories import ContactFactory
from crm.tests.factories import TicketFactory
from login.tests.fixture import perm_check
from sales_order.tests.factories import SalesOrderFactory
from sales_order_ticket.tests.factories import SalesOrderTicketFactory


@pytest.mark.django_db
def test_contact_detail(perm_check):
    contact = ContactFactory()
    url = reverse("contact.detail", args=[contact.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_dash(perm_check):
    perm_check.staff(reverse("project.dash"))


@pytest.mark.django_db
def test_home(perm_check):
    perm_check.anon(reverse("project.home"))


@pytest.mark.django_db
def test_sales_order_detail(perm_check):
    sales_order = SalesOrderFactory(contact=ContactFactory())
    SalesOrderTicketFactory(
        sales_order=sales_order,
        ticket=TicketFactory(contact=sales_order.contact),
    )
    perm_check.staff(reverse("sales.order.detail", args=[sales_order.pk]))


@pytest.mark.django_db
def test_settings(perm_check):
    perm_check.staff(reverse("project.settings"))
