# -*- encoding: utf-8 -*-
import csv
import pytest

from datetime import date, time
from django.core.management import call_command

from contact.tests.factories import ContactFactory
from crm.tests.factories import TicketFactory
from invoice.tests.factories import TimeRecordFactory
from login.tests.factories import UserFactory
from report.tests.helper import run_test_report
from sales_order.tests.factories import SalesOrderFactory
from sales_order_ticket.reports import (
    sales_order_time_analysis,
    SalesOrderTicketHoursReport,
)
from sales_order_ticket.tests.factories import SalesOrderTicketFactory


@pytest.mark.django_db
def test_sales_order_time_analysis():
    contact = ContactFactory(user=UserFactory(username="green"))
    sales_order = SalesOrderFactory(contact=contact, title="Hosting")
    ticket = TicketFactory(contact=contact, title="Build Server")
    SalesOrderTicketFactory(sales_order=sales_order, ticket=ticket)
    user = UserFactory(username="patrick")
    TimeRecordFactory(
        ticket=ticket,
        date_started=date(2022, 3, 16),
        start_time=time(11, 0),
        end_time=time(11, 30),
        user=user,
    )
    result = sales_order_time_analysis()
    assert {
        "green": {
            "{}-Hosting".format(sales_order.sales_order_number): [
                (ticket.number, "Build Server", "patrick", "00:00", "00:30"),
            ]
        }
    } == result


@pytest.mark.django_db
def test_sales_order_ticket_hours():
    call_command("init-app-sales-order-ticket")
    contact = ContactFactory(company_name="Green")
    ticket_1 = TicketFactory(contact=contact, title="ticket-1")
    ticket_2 = TicketFactory(contact=contact, title="ticket-2")
    ticket_3 = TicketFactory(contact=contact, title="ticket-3")
    user_1 = UserFactory(username="patrick")
    user_2 = UserFactory(username="matt")
    sales_order = SalesOrderFactory(contact=contact)
    SalesOrderTicketFactory(sales_order=sales_order, ticket=ticket_1)
    SalesOrderTicketFactory(sales_order=sales_order, ticket=ticket_3)
    TimeRecordFactory(
        ticket=ticket_1,
        date_started=date(2017, 1, 16),
        start_time=time(11, 0),
        end_time=time(11, 30),
        user=user_1,
    )
    TimeRecordFactory(
        billable=False,
        ticket=ticket_2,
        date_started=date(2016, 12, 1),
        start_time=time(11, 0),
        end_time=time(11, 15),
        user=user_2,
    )
    TimeRecordFactory(
        billable=False,
        ticket=ticket_3,
        date_started=date(2015, 12, 31),
        start_time=time(11, 0),
        end_time=time(11, 20),
        user=user_2,
    )
    parameters = {"sales_order_pk": sales_order.pk}
    first_row, data = run_test_report(
        UserFactory(), SalesOrderTicketHoursReport.REPORT_SLUG, parameters
    )
    assert [
        "Contact",
        "Ticket",
        "Ticket",
        "Year",
        "Month",
        "User",
        "Charge",
        "Fixed",
        "Non-Charge",
        "Invoiced",
        "Pending",
    ] == first_row
    assert [
        [
            "Green",
            ticket_1.number,
            "ticket-1",
            "2017",
            "1",
            "patrick",
            "00:30",
            "00:00",
            "00:00",
            "00:00",
            "00:30",
        ],
        [
            "Green",
            ticket_3.number,
            "ticket-3",
            "2015",
            "12",
            "matt",
            "00:00",
            "00:00",
            "00:20",
            "00:00",
            "00:00",
        ],
    ] == data
