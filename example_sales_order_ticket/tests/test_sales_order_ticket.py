# -*- encoding: utf-8 -*-
import pytest

from django.db import IntegrityError

from contact.tests.factories import ContactFactory
from crm.models import Funded, KanbanCard, KanbanColumn
from crm.tests.factories import FundedFactory, KanbanCardFactory, TicketFactory
from login.tests.factories import UserFactory
from invoice.tests.factories import TimeRecordFactory
from sales_order.tests.factories import SalesOrderFactory
from sales_order_ticket.models import SalesOrderTicket
from sales_order_ticket.tests.factories import SalesOrderTicketFactory


@pytest.mark.django_db
def test_check_no_sales_order_chargeable_time():
    """A ticket with chargeable time, must have a sales order."""
    contact = ContactFactory()
    sales_order = SalesOrderFactory(contact=contact)
    ticket_1 = TicketFactory(contact=contact)
    ticket_2 = TicketFactory(contact=contact)
    ticket_3 = TicketFactory(contact=contact)
    SalesOrderTicketFactory(ticket=ticket_2, sales_order=sales_order)
    TimeRecordFactory(billable=True, ticket=ticket_1)
    TimeRecordFactory(billable=True, ticket=ticket_1)
    TimeRecordFactory(billable=True, ticket=ticket_2)
    TimeRecordFactory(billable=True, ticket=ticket_3)
    assert set([ticket_1.pk, ticket_3.pk]) == set(
        SalesOrderTicket.objects.check_tickets()
    )


@pytest.mark.django_db
def test_current():
    contact_1 = ContactFactory()
    sales_order_ticket_1 = SalesOrderTicketFactory(
        sales_order=SalesOrderFactory(number=1, contact=contact_1),
        ticket=TicketFactory(contact=contact_1),
    )
    contact_2 = ContactFactory()
    sales_order_ticket_2 = SalesOrderTicketFactory(
        sales_order=SalesOrderFactory(number=2, contact=contact_2),
        ticket=TicketFactory(contact=contact_2),
    )
    sales_order_ticket_2.set_deleted(UserFactory())
    contact_3 = ContactFactory()
    sales_order_ticket_3 = SalesOrderTicketFactory(
        sales_order=SalesOrderFactory(number=3, contact=contact_3),
        ticket=TicketFactory(contact=contact_3),
    )
    sales_order_4 = SalesOrderFactory(number=4, contact=contact_1)
    sales_order_ticket_4 = SalesOrderTicketFactory(
        sales_order=sales_order_4, ticket=TicketFactory(contact=contact_1)
    )
    sales_order_ticket_5 = SalesOrderTicketFactory(
        sales_order=sales_order_4, ticket=TicketFactory(contact=contact_1)
    )
    assert [
        sales_order_ticket_5.pk,
        sales_order_ticket_4.pk,
        sales_order_ticket_3.pk,
        sales_order_ticket_1.pk,
    ] == [x.pk for x in SalesOrderTicket.objects.current()]
    assert [
        sales_order_ticket_5.pk,
        sales_order_ticket_4.pk,
        sales_order_ticket_1.pk,
    ] == [x.pk for x in SalesOrderTicket.objects.current(contact=contact_1)]
    assert [sales_order_ticket_5.pk, sales_order_ticket_4.pk] == [
        x.pk
        for x in SalesOrderTicket.objects.current(sales_order=sales_order_4)
    ]


@pytest.mark.django_db
def test_unique():
    """A sales order can have several tickets."""
    sales_order = SalesOrderFactory(contact=ContactFactory())
    SalesOrderTicketFactory(sales_order=sales_order, ticket=TicketFactory())
    SalesOrderTicketFactory(sales_order=sales_order, ticket=TicketFactory())


@pytest.mark.django_db
def test_unique_for_a_ticket():
    """A ticket can only have one sales order."""
    ticket = TicketFactory()
    SalesOrderTicketFactory(
        sales_order=SalesOrderFactory(contact=ContactFactory()), ticket=ticket
    )
    with pytest.raises(IntegrityError) as e:
        SalesOrderTicketFactory(
            sales_order=SalesOrderFactory(contact=ContactFactory()),
            ticket=ticket,
        )
    assert "duplicate key value violates unique constraint" in str(e.value)


@pytest.mark.django_db
def test_for_ticket():
    ticket = TicketFactory()
    x = SalesOrderTicketFactory(
        sales_order=SalesOrderFactory(contact=ContactFactory()), ticket=ticket
    )
    assert x == SalesOrderTicket.objects.for_ticket(ticket)


@pytest.mark.django_db
def test_for_ticket_does_not_exist():
    ticket = TicketFactory()
    assert SalesOrderTicket.objects.for_ticket(ticket) is None


@pytest.mark.django_db
def test_ordering():
    contact = ContactFactory()
    SalesOrderTicketFactory(
        sales_order=SalesOrderFactory(number=1, title="X", contact=contact),
        ticket=TicketFactory(contact=contact, title="B"),
    )
    SalesOrderTicketFactory(
        sales_order=SalesOrderFactory(number=2, title="Y", contact=contact),
        ticket=TicketFactory(contact=contact, title="A"),
    )
    assert ["Y", "X"] == [
        x.sales_order.title for x in SalesOrderTicket.objects.all()
    ]


@pytest.mark.django_db
def test_str():
    contact = ContactFactory()
    ticket = TicketFactory(contact=contact, title="Apple")
    x = SalesOrderTicketFactory(
        sales_order=SalesOrderFactory(number=1, title="Order", contact=contact),
        ticket=ticket,
    )
    assert "1 Order for {} Apple".format(ticket.number) == str(x)


@pytest.mark.django_db
def test_tickets_without_sales_order(django_assert_num_queries):
    contact = ContactFactory()
    ticket_1 = TicketFactory(contact=contact, title="A", user=UserFactory())
    TimeRecordFactory(billable=True, ticket=ticket_1)
    SalesOrderTicketFactory(
        sales_order=SalesOrderFactory(number=1, title="X", contact=contact),
        ticket=ticket_1,
    )
    ticket_2 = TicketFactory(contact=contact, title="B")
    TimeRecordFactory(billable=True, ticket=ticket_2, user=UserFactory())
    ticket_3 = TicketFactory(contact=contact, title="C")
    TimeRecordFactory(billable=False, ticket=ticket_3, user=UserFactory())
    ticket_4 = TicketFactory(contact=contact, title="D")
    TimeRecordFactory(billable=True, ticket=ticket_4, user=UserFactory())
    with django_assert_num_queries(5):
        qs = SalesOrderTicket.objects.tickets_without_sales_order().order_by(
            "title"
        )
    assert ["B", "D"] == [x.title for x in qs]
