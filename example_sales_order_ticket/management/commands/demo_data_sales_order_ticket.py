# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Sales Order Ticket Demo Data"

    def handle(self, *args, **options):
        self.stdout.write(self.help)
        self.stdout.write("Complete: {}".format(self.help))
