# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.views.generic import DetailView, ListView, TemplateView

from base.view_utils import BaseMixin
from contact.views import ContactDetailMixin
from crm.views import TicketDetailMixin


class ContactDetailView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ContactDetailMixin,
    BaseMixin,
    DetailView,
):
    template_name = "example/contact_detail.html"


class DashView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, TemplateView
):
    template_name = "example/dash.html"


class HomeView(BaseMixin, TemplateView):
    template_name = "example/home.html"


class SettingsView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, TemplateView
):
    template_name = "example/settings.html"


class TicketDetailView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    TicketDetailMixin,
    BaseMixin,
    ListView,
):
    template_name = "example/ticket_detail.html"
